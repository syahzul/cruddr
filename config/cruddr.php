<?php

return [
    'controller' => [
        // Total item per page for paginated views
        'pagination' => [
            'total' => 15
        ],
        // View file name, without .blade.php
        'view' => [
            'index' => 'index',
            'create' => 'create',
            'edit' => 'edit',
            'show' => 'show',
        ]
    ]
];