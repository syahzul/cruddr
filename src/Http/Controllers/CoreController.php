<?php

namespace Syahzul\Cruddr\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Syahzul\Cruddr\Http\Traits\ValidateInputs;
use Syahzul\Cruddr\Models\CoreModel;

abstract class CoreController extends Controller
{
    use ValidateInputs;

    /**
     * @var CoreModel
     */
    protected $model;

    /**
     * @var string resource name
     */
    protected $resourcesName;

    /**
     * @var array Store resource validation rules
     */
    protected $storeRules = [];

    /**
     * @var array Update resource validation rules
     */
    protected $updateRules = [];

    /**
     * @var string  Messages
     */
    protected $messages = [
        'validation_error' => 'Opss!! An error has occured. Please check your inputs before proceeding.',
        'create_success' => 'Resource has been created!',
        'delete_success' => 'Resource has been deleted!',
        'update_success' => 'Resource has been updated!',
    ];

    /**
     * @var string Plural name
     */
    protected $plural;

    /**
     * @var string Singular name
     */
    protected $singular;

    /**
     * @var array View files
     */
    protected $viewFile = [
        'create' => 'create',
        'edit' => 'edit',
        'index' => 'index',
        'view' => 'view',
    ];

    /**
     * @var View
     */
    protected $view;

    protected $vars = [];

    /**
     * CoreController constructor.
     *
     * @param CoreModel $model
     */
    public function __construct(CoreModel $model)
    {
        $this->model = $model;
    }

    /**
     * List all resources
     *
     * @param Request $request
     * @return View
     */
    public function index(Request $request)
    {
        $resources = $this->filter($request);

        $this->view = view($this->plural.'.'.$this->viewFile['index'])
            ->with([
                $this->resourcesName => $resources
            ]);

        return $this->view;
    }

    /**
     * Create resource form
     *
     * @return View
     */
    public function create()
    {
        $this->beforeRenderFormCreate();

        return view($this->plural.'.'.$this->viewFile['create'])
            ->with($this->vars);
    }

    /**
     * Create a resource
     *
     * @param Request $request
     * @return RedirectResponse|\Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validates($request, $this->storeRules);

        $input = $this->beforeStore($request->all());

        $resource = $this->model->create($input);

        $this->afterStore($resource);

        flash()->success($this->messages['create_success']);

        return redirect()->route($this->plural.'.index');
    }

    /**
     * Edit resource form
     *
     * @param $id
     * @return View
     * @internal param CoreModel $resource
     */
    public function edit($id)
    {
        $resource = $this->findResource($id);

        $this->vars[$this->singular] = $resource;

        $this->beforeRenderFormEdit();

        return view($this->plural.'.'.$this->viewFile['edit'])
            ->with($this->vars);
    }

    /**
     * Update a resource
     *
     * @param Request $request
     * @param CoreModel $resource
     * @return RedirectResponse
     */
    public function update(Request $request, CoreModel $resource)
    {
        $this->validates($request, $this->storeRules);

        $input = $this->beforeUpdate($request->all());

        $resource->update($input);

        $this->afterUpdate($resource);

        flash()->success($this->messages['update_success']);

        return redirect()->route($this->plural.'.index');
    }

    /**
     * Delete a resource
     *
     * @param $id
     * @return RedirectResponse
     */
    public function delete($id)
    {
        $resource = $this->findResource($id);

        $this->beforeDelete($resource);

        $resource->delete();

        $this->afterDelete($id);

        flash()->success($this->messages['delete_success']);

        return redirect()->route($this->plural.'.index');
    }

    /**
     * Filter requests
     *
     * @param Request $request
     * @return mixed
     */
    protected function filter(Request $request)
    {
        return $this->model->paginate(config('ppzcore::core.controller.pagination.total'));
    }

    /**
     * Find a resource by ID
     *
     * @param $id
     * @return mixed
     */
    protected function findResource($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * Run action before render form create
     */
    protected function beforeRenderFormCreate()
    {
    }

    /**
     * Run action before render form edit
     */
    protected function beforeRenderFormEdit()
    {
    }

    /**
     * Modify input array before store resource
     *
     * @param $input
     * @return mixed
     */
    protected function beforeStore($input)
    {
        return $input;
    }

    /**
     * Run action after storing resource
     *
     * @param $resource
     * @return mixed
     * @internal param $input
     */
    protected function afterStore($resource)
    {
        return $resource;
    }

    /**
     * Modify input array before update resource
     *
     * @param $input
     * @return mixed
     */
    protected function beforeUpdate($input)
    {
        return $input;
    }

    /**
     * Run action after updating resource
     *
     * @param $resource
     * @return mixed
     */
    protected function afterUpdate($resource)
    {
        return $resource;
    }

    /**
     * Do action before deleting the resource
     *
     * @param $resource
     * @return mixed
     */
    protected function beforeDelete($resource)
    {
        return $resource;
    }

    /**
     * Do action after deleting resource
     *
     * @param $id
     * @return mixed
     */
    protected function afterDelete($id)
    {
        return $id;
    }

}