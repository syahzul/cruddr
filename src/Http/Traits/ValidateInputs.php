<?php

namespace Syahzul\Cruddr\Http\Traits;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

trait ValidateInputs
{
    /**
     * Validate inputs
     *
     * @param Request $request
     * @param $rules
     * @return RedirectResponse|bool
     */
    protected function validates(Request $request, $rules)
    {
        $validation = Validator::make($request->all(), $rules);

        if ($validation->fails()) {
            flash()->error($this->messages['validation_error']);

            return redirect()->back()->withInput()->withErrors($validation);
        }

        return true;
    }
}