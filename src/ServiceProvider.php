<?php

namespace Syahzul\Cruddr;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    public function boot()
    {
        $this->publishConfig();
    }

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/core.php', 'cruddr'
        );
    }

    protected function publishConfig()
    {
        $this->publishes([
            __DIR__.'/../config/cruddr.php' => config_path('cruddr.php'),
        ]);
    }
}